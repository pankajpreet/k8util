package main

import (
	"fmt"
	"k8util/tools"
	"net/http"
)

func main() {

	// create a new handler
	createdch := make(chan string)
	deletedch:= make(chan string)
	go tools.ProcessScaledPods(createdch, deletedch )
	handler := http.NewServeMux()
	handler.HandleFunc("/start", func(writer http.ResponseWriter, request *http.Request) {
		data := []byte("Started !") // slice of bytes
		// write `data` to response
		createdch <- "start"
		deletedch <- "start"
		writer.Write(data)
	})

	// listen and serve
	err := http.ListenAndServe(":9000", handler)
	if err!=nil{
		fmt.Println(err.Error())
	}
}
