module k8util

go 1.14

require (
	github.com/onsi/gomega v1.7.0
	k8s.io/api v0.17.8
	k8s.io/apimachinery v0.17.8
	k8s.io/client-go v0.17.8
)
