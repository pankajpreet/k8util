package tools

import (
	"encoding/json"
	"flag"
	"fmt"
	"k8util/framework"
	"path/filepath"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	//
	// Uncomment to load all auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth"
	//
	// Or uncomment to load specific auth plugins
	// _ "k8s.io/client-go/plugin/pkg/client/auth/azure"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	// _ "k8s.io/client-go/plugin/pkg/client/auth/openstack"
)

func ListPodOS() {

	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	fw := framework.Framework{ClientSet: clientset, Config: config}

	namepacevspoddetails := make(map[string]map[string]string, 0)
	ns, _ := clientset.CoreV1().Namespaces().List(v1.ListOptions{})
	for _, nmsp := range ns.Items {
		if nmsp.Name != "kube-system" && nmsp.Name != "kube-public" && nmsp.Name != "kube-node-lease" {
			poddetails := namepacevspoddetails[nmsp.Name]
			if poddetails == nil {
				poddetails = map[string]string{}
			}
			pods, err := clientset.CoreV1().Pods(nmsp.Name).List(v1.ListOptions{})
			if err != nil {
				panic(err)
			}
			// print pods
			for _, pod := range pods.Items {
				fmt.Println(nmsp.Name, pod.Name)
				out, erout, err := fw.ExecWithOptions(framework.ExecOptions{
					Command:            []string{"/bin/sh", "-c", "cat /etc/os-release"},
					Namespace:          nmsp.Name,
					PodName:            pod.Name,
					ContainerName:      pod.Spec.Containers[0].Name,
					Stdin:              nil,
					CaptureStdout:      true,
					CaptureStderr:      true,
					PreserveWhitespace: false,
				})
				if err != nil {
					println(erout)
					panic(err)
				}
				// out = strings.ReplaceAll(out, "\n", ",")
				// out = strings.ReplaceAll(out, "\"", "")
				poddetails[pod.Name] = out
				namepacevspoddetails[nmsp.Name] = poddetails
			}
		}
	}
	jsonStr, err := json.Marshal(namepacevspoddetails)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(jsonStr))
}
