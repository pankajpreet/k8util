package tools

type PodDetails struct{

	Name string
	Timestamp int64
	Count int
}