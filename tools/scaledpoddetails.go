package tools

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
	"os"
	"path/filepath"
	"time"
)

var podsCreated = 0
var podsDeleted = 0

func ProcessScaledPods(createdch chan string, deletedch chan string) {
	fmt.Println("Inside Process Scaled Pods............")
	var kubeconfig *string
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		panic(err.Error())
	}
	createdPodDetailsFile := "/tmp/podsCreated.txt"
	deletedPodDetailsFile := "/tmp/podsDeleted.txt"

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	factory := informers.NewSharedInformerFactoryWithOptions(clientset, 0, informers.WithNamespace("default"))
	informer := factory.Core().V1().Pods().Informer()
	stopper := make(chan struct{})
	createdPodsch := make(chan PodDetails, 100)
	deletedPodsch := make(chan PodDetails, 100)
	closech := make(chan struct{})
	go processPodCreatedDetails(createdPodsch, closech, createdPodDetailsFile, createdch)
	go processPodDeletedDetails(deletedPodsch, closech, deletedPodDetailsFile, deletedch)
	defer close(stopper)
	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			mObj := obj.(v1.Object)
			createdPodsch <- PodDetails{ Name: mObj.GetName(),
				Timestamp: mObj.GetCreationTimestamp().UnixNano() / 1000000,
			}
		},
		DeleteFunc: func(obj interface{}) {
			mObj := obj.(v1.Object)
			deletedPodsch <- PodDetails{Name: mObj.GetName(),
				Timestamp: mObj.GetDeletionTimestamp().UnixNano() / 1000000,
			}
		},
	})
	go printCountStats()
	informer.Run(stopper)
}

func printCountStats(){
	for{
		time.Sleep(5 * time.Second)
		fmt.Printf("Pods Created: %d, Pods Deleted: %d, time: %s\n", podsCreated, podsDeleted, time.Now().String())
	}
}

func processPodCreatedDetails(podDetails chan PodDetails, closechan chan struct{}, file string, proc chan string) {
	fmt.Println("Inside Process created Pods............")
	f, err:= os.Create(file)
	if err != nil{
		fmt.Println("There is error while writing created pods details to file. Failed to get file handle")
	}
	writer:= bufio.NewWriter(f)
	startProcessing := false
	startDumping := false
	for {
		select {
		case poddetail := <- podDetails:
			if startProcessing {
				fmt.Println("Start Collecting.................")
				startDumping = true
				startProcessing = false
				podsCreated = 0
			}
			if startDumping {
				podsCreated++
				poddetail.Count = podsCreated
				data, err:= json.Marshal(poddetail)
				if err != nil{
					fmt.Println("There is error while writing created pods details to file.")
				}
				writer.WriteString(string(data))
			}
		case <- proc:
			fmt.Println("recvd created channel callback............")
			startProcessing = true
		case <-closechan:
			close(podDetails)
			close(closechan)
			break
		}
	}
	writer.Flush()

}

func processPodDeletedDetails(podDetails chan PodDetails, closechan chan struct{}, file string, proc chan string) {
	f, err:= os.Create(file)
	if err != nil{
		fmt.Println("There is error while writing pods deleted details to file. Failed to get file handle")
	}
	writer:= bufio.NewWriter(f)
	startProcessing := false
	startDumping := false
	for {
		select {
		case poddetail := <- podDetails:
			if startProcessing {
				startDumping = true
				startProcessing = false
				podsDeleted = 0
			}
			if startDumping {
				podsDeleted++
				poddetail.Count = podsDeleted
				data, err:= json.Marshal(poddetail)
				if err != nil{
					fmt.Println("There is error while writing deleted pods details to file.")
				}
				writer.WriteString(string(data))
			}
		case <- proc:
			fmt.Println("recvd deleted channel callback............")
			startProcessing = true
		case <-closechan:
			close(podDetails)
			close(closechan)
			break
		}
	}
	writer.Flush()

}
